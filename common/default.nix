{
  imports = [
    ./bat.nix
    # ./fish.nix
    ./helix.nix
    # ./kitty.nix
    ./starship.nix
    ./wezterm.nix
    ./zellij.nix
    ./nu.nix
    ./ruff.nix
    ./git.nix
    ./yazi.nix
    ./direnv.nix
  ];
}
