{
  bg0 = "#ebdbb2";
  bg1 = "#efe0b7";
  bg = "#f2e5bc";
  bg2 = "#ebdbb2";
  bg3 = "#e6d5ae";
  bg4 = "#e0cfa9";
  bg5 = "#d5c4a1";
  bg6 = "#c9b99a";
  bg7 = "#bdae93";
  bg8 = "#a89984";
  bg9 = "#928374";
  gray0 = "#a89984";
  gray1 = "#928374";
  gray2 = "#7c6f64";

  fg0 = "#514036";
  fg = "#514036";
  fg1 = "#6f4f3c";

  red = "#af2528";
  orange = "#b94c07";
  yellow = "#b4730e";
  green = "#72761e";
  aqua = "#477a5b";
  blue = "#266b79";
  purple = "#924f79";

  lightred = "#bf3538";
  lightorange = "#c26112";
  lightyellow = "#c8841e";
  lightgreen = "#82872e";
  lightaqua = "#5a7e6c";
  lightblue = "#3c7087";
  lightpurple = "#a35f85";
}
