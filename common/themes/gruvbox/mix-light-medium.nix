{
  bg0 = "#f2e5bc";
  bg1 = "#f6ebc1";
  bg = "#fbf1c7";
  bg2 = "#f2e5bc";
  bg3 = "#eee0b7";
  bg4 = "#ebdbb2";
  bg5 = "#e0cfa9";
  bg6 = "#d5c4a1";
  bg7 = "#c9b99a";
  bg8 = "#bdae93";
  bg9 = "#a89984";
  gray0 = "#a89984";
  gray1 = "#928374";
  gray2 = "#7c6f64";

  fg0 = "#514036";
  fg = "#514036";
  fg1 = "#6f4f3c";

  red = "#af2528";
  orange = "#b94c07";
  yellow = "#b4730e";
  green = "#72761e";
  aqua = "#477a5b";
  blue = "#266b79";
  purple = "#924f79";

  lightred = "#bf3538";
  lightorange = "#c26112";
  lightyellow = "#c8841e";
  lightgreen = "#82872e";
  lightaqua = "#5a7e6c";
  lightblue = "#3c7087";
  lightpurple = "#a35f85";
}
