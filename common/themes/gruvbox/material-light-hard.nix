{
  bg0 = "#f2e5bc";
  bg1 = "#f3eac7";
  bg = "#f9f5d7";
  bg2 = "#fbf1c7";
  bg3 = "#f2e5bc";
  bg4 = "#f2e5bc";
  bg5 = "#ebdbb2";
  bg6 = "#e0cfa9";
  bg7 = "#d5c4a1";
  bg8 = "#c9b99a";
  bg9 = "#bdae93";
  gray0 = "#a89984";
  gray1 = "#928374";
  gray2 = "#7c6f64";

  fg0 = "#4f3829";
  fg = "#654735";
  fg1 = "#6f4f3c";

  red = "#c14a4a";
  orange = "#c35e0a";
  yellow = "#b47109";
  green = "#6c782e";
  aqua = "#4c7a5d";
  blue = "#45707a";
  purple = "#945e80";

  lightred = "#cc5a5a";
  lightorange = "#cf7014";
  lightyellow = "#c1851e";
  lightgreen = "#7c8938";
  lightaqua = "#5c8b6f";
  lightblue = "#547e87";
  lightpurple = "#a56e8c";
}
