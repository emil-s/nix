{
  bg0 = "#070808";
  bg1 = "#131414";
  bg = "#202020";
  bg2 = "#2a2827";
  bg3 = "#2e2c2b";
  bg4 = "#32302f";
  bg5 = "#3d3835";
  bg6 = "#46403d";
  bg7 = "#514945";
  bg8 = "#5a524c";
  bg9 = "#665c54";
  gray0 = "#7c6f64";
  gray1 = "#928374";
  gray2 = "#a89984";

  fg0 = "#ddc7a1";
  fg = "#d4be98";
  fg1 = "#c5b18d";

  red = "#ea6962";
  orange = "#e78a4e";
  yellow = "#d8a657";
  green = "#a9b665";
  aqua = "#89b482";
  blue = "#7daea3";
  purple = "#d3869b";

  lightred = "#ec7872";
  lightorange = "#e8975a";
  lightyellow = "#daa76b";
  lightgreen = "#bac76f";
  lightaqua = "#a2c788";
  lightblue = "#95c9a6";
  lightpurple = "#d499af";
}
