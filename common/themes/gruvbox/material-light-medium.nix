{
  bg0 = "#ebdbb2";
  bg1 = "#efe0b7";
  bg = "#f2e5bc";
  bg2 = "#ebdbb2";
  bg3 = "#e6d5ae";
  bg4 = "#e0cfa9";
  bg5 = "#d5c4a1";
  bg6 = "#c9b99a";
  bg7 = "#bdae93";
  bg8 = "#a89984";
  bg9 = "#928374";
  gray0 = "#a89984";
  gray1 = "#928374";
  gray2 = "#7c6f64";

  fg0 = "#4f3829";
  fg = "#654735";
  fg1 = "#6f4f3c";

  red = "#c14a4a";
  orange = "#c35e0a";
  yellow = "#b47109";
  green = "#6c782e";
  aqua = "#4c7a5d";
  blue = "#45707a";
  purple = "#945e80";

  lightred = "#cc5a5a";
  lightorange = "#cf7014";
  lightyellow = "#c1851e";
  lightgreen = "#7c8938";
  lightaqua = "#5c8b6f";
  lightblue = "#547e87";
  lightpurple = "#a56e8c";
}
