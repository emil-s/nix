{
  bg0 = "#181919";
  bg1 = "#242424";
  bg = "#32302f";
  bg2 = "#3c3836";
  bg3 = "#403c3a";
  bg4 = "#45403d";
  bg5 = "#504945";
  bg6 = "#5a524c";
  bg7 = "#665c54";
  bg8 = "#7c6f64";
  bg9 = "#928374";
  gray0 = "#7c6f64";
  gray1 = "#928374";
  gray2 = "#a89984";

  fg0 = "#ddc7a1";
  fg = "#d4be98";
  fg1 = "#c5b18d";

  red = "#ea6962";
  orange = "#e78a4e";
  yellow = "#d8a657";
  green = "#a9b665";
  aqua = "#89b482";
  blue = "#7daea3";
  purple = "#d3869b";

  lightred = "#ec7872";
  lightorange = "#e8975a";
  lightyellow = "#daa76b";
  lightgreen = "#bac76f";
  lightaqua = "#a2c788";
  lightblue = "#95c9a6";
  lightpurple = "#d499af";
}
