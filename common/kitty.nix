{
  programs.kitty = {
    enable = true;

    theme = "Gruvbox Material Dark Hard";

    settings = {
      font_size = 12;
      font_family = "CaskaydiaCove NF Mono";
    };
  };
}
